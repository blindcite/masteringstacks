SOURCES= \
	book.tex \
	cryptography.tex \
	smart-contract-security.tex \
	smart-contracts.tex \
	stacks-basics.tex \
	tokens.tex \
	transactions.tex \
	what-is-stacks.tex \


book.pdf: $(SOURCES)
	pdflatex book.tex
	pdflatex book.tex
	pdflatex book.tex
